-- Copyright 2022 triallax
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

-- This script adds a `v` binding that functions the same as mpv's built-in `v`
-- binding, except that it enables subtitles in case they weren't already
-- enabled (the built-in `v` only hides/shows subtitles; it won't enable them
-- if they weren't already enabled). This is useful because it selects the
-- default subtitle format, saving the effort of cycling through the subtitle
-- formats for the one I want (usually English subtitles) using `j` and `J`.

mp.add_key_binding("v", function()
	-- `mp.command` is used here instead of `mp.set_property` so that a
	-- message is shown on the OSC.
	if mp.get_property("sub") == "no" then
		mp.command("set sub auto")
	else
		mp.command("cycle sub-visibility")
	end
end)
