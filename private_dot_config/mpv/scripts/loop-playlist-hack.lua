-- Copyright 2022 triallax
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--     http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

-- This script fixes `--loop-playlist` refetching a playlist entry each time it
-- loops.

if mp.get_property("loop-playlist") == "inf" then
	local function on_count_changed(name, value)
		-- Using `--loop-playlist` with a single playlist entry causes it to
		-- get refetched on every loop. This doesn't happen with `--loop`.
		if value == 1 then
			mp.set_property("loop", "inf")
			mp.set_property("loop-playlist", "no")
		else
			mp.set_property("loop", "no")
			mp.set_property("loop-playlist", "inf")
		end
	end

	-- Why listen to the property, you ask? Well, if MPV is passed e.g. a
	-- YouTube playlist, it will initially be counted as one item, until its
	-- items are loaded.
	mp.observe_property("playlist-count", "number", on_count_changed)
end
