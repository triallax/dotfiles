complete -c spdrn -f

complete -c spdrn -n __fish_use_subcommand -a ls -d 'List all runs'
complete -c spdrn -n __fish_use_subcommand -a pb -d 'Show info about personal best'
complete -c spdrn -n __fish_use_subcommand -a sob -d 'Show info about the sum-of-best run'

complete -c spdrn -s h -l help -d 'Display help and exit'

complete -c spdrn -n '__fish_seen_subcommand_from pb sob' -s l -l long -d 'Show detailed info'
