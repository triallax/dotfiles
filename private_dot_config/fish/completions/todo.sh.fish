# TODO: submit this upstream (where, Fish or todo.txt?) when complete.

# See https://github.com/todotxt/todo.txt-cli/blob/master/todo_completion for
# guidance.

complete -c todo.sh -f

set -l subcommands add addm addto append archive command deduplicate rm del depri done help list listall listaddons listcon listfile listpri listproj move prepend pri replace report shorthelp

complete -c todo.sh -n '! __fish_seen_subcommand_from $subcommands' -s @ -d 'Hide context names'
complete -c todo.sh -n '! __fish_seen_subcommand_from $subcommands' -s + -d 'Hide project names in list output'
complete -c todo.sh -n '! __fish_seen_subcommand_from $subcommands' -s c -d 'Enable color mode'
complete -c todo.sh -n '! __fish_seen_subcommand_from $subcommands' -s d -rFd 'Set config file path'

# TODO: improve these descriptions.
complete -c todo.sh -n __fish_use_subcommand -a add -d 'Add item to todo.txt'
complete -c todo.sh -n __fish_use_subcommand -a addm -d 'Add multiple items (separated by newlines) to todo.txt'
complete -c todo.sh -n __fish_use_subcommand -a addto -d 'Add item to given file in todo directory'
complete -c todo.sh -n __fish_use_subcommand -a append -d 'Append given text to a task'
complete -c todo.sh -n __fish_use_subcommand -a archive -d 'Move done tasks to done.txt'
complete -c todo.sh -n __fish_use_subcommand -a command -d 'Run arguments using todo.sh builtins only'
complete -c todo.sh -n __fish_use_subcommand -a deduplicate -d 'Remove duplicate lines from todo.txt'
complete -c todo.sh -n __fish_use_subcommand -a 'rm del' -d 'Delete the given task from todo.txt'
complete -c todo.sh -n __fish_use_subcommand -a depri -d 'Deprioritizes given task in todo.txt'
complete -c todo.sh -n __fish_use_subcommand -a done -d 'Mark given task as done in todo.txt'
complete -c todo.sh -n __fish_use_subcommand -a help -d 'Display help and exit'
complete -c todo.sh -n __fish_use_subcommand -a list -d 'Display undone tasks in todo.txt'
complete -c todo.sh -n __fish_use_subcommand -a listall -d 'Display tasks from todo.txt and done.txt'
complete -c todo.sh -n __fish_use_subcommand -a listaddons -d 'List all added and overridden actions in the actions directory'
complete -c todo.sh -n __fish_use_subcommand -a listcon -d 'List task contexts in todo.txt'
complete -c todo.sh -n __fish_use_subcommand -a listfile -d 'Display all tasks from given file in todo directory'
complete -c todo.sh -n __fish_use_subcommand -a listpri -d 'Display all tasks with given priorities'
complete -c todo.sh -n __fish_use_subcommand -a listproj -d 'List projects in todo.txt'
complete -c todo.sh -n __fish_use_subcommand -a move -d 'Move task'
complete -c todo.sh -n __fish_use_subcommand -a prepend -d 'Prepend given text to a todo'
complete -c todo.sh -n __fish_use_subcommand -a pri -d 'Add priority to task in todo.txt'
complete -c todo.sh -n __fish_use_subcommand -a replace -d 'Replace task text'
complete -c todo.sh -n __fish_use_subcommand -a report -d 'Write report to report.txt in todo directory'
complete -c todo.sh -n __fish_use_subcommand -a shorthelp -d 'Display short help and exit'

function __todo_sh_complete_task_number
    TODOTXT_VERBOSE=0 command todo.sh -p command ls | string replace ' ' \t
end

complete -c todo.sh -n '__fish_seen_subcommand_from append app del rm move mv prepend prep pri p replace' -n '[ (count (commandline -poc)) -eq 2 ]' -ka '(__todo_sh_complete_task_number)'
complete -c todo.sh -n '__fish_seen_subcommand_from depri dp done do' -ka '(__todo_sh_complete_task_number)'
