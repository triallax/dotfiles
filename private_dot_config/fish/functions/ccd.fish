# This doesn't spawn a new shell, whereas chezmoi cd has to.
# See https://www.chezmoi.io/user-guide/frequently-asked-questions/design/#why-does-chezmoi-cd-spawn-a-shell-instead-of-just-changing-directory.
function ccd
    cd (chezmoi source-path)
end
