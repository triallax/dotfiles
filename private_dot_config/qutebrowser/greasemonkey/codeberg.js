// ==UserScript==
// @match https://codeberg.org/*
//
// @run-at document-start
// @grant none
// @qute-js-world jseval
// ==/UserScript==

(() => {
  new MutationObserver((mutations, observer) => {
    if (document.head) {
      const style = document.createElement("style");

      style.textContent = `
      #user-heatmap,
      #user-heatmap + .divider,
      .repo-buttons .label[href$="/watchers"],
      #navbar > .item[href="/explore/repos"] {
        display: none !important;
      }`;

      document.head.append(style);

      observer.disconnect();
    }
  }).observe(document, { childList: true });
})();
