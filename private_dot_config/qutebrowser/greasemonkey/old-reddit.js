// ==UserScript==
// @match https://old.reddit.com/*
//
// @run-at document-start
// @grant none
// @qute-js-world jseval
// ==/UserScript==

(() => {
  new MutationObserver((mutations, observer) => {
    if (document.head) {
      const style = document.createElement("style");

      style.textContent = `
      #header-bottom-right,
      .listingsignupbar,
      #login_login-main,
      .premium-banner,
      .spacer:has(.submit),
      #sr-header-area,
      .commentsignupbar,
      .footer-parent,
      .login-required {
        display: none !important;
      }`;

      document.head.append(style);

      observer.disconnect();
    }
  }).observe(document, { childList: true });
})();
