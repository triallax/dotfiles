// ==UserScript==
// @match https://stackoverflow.com/*
// @match https://askubuntu.com/*
// @match https://superuser.com/*
// @match https://serverfault.com/*
// @match https://*.stackexchange.com/*
//
// @run-at document-start
// @grant none
// @qute-js-world jseval
// ==/UserScript==

(() => {
  new MutationObserver((mutations, observer) => {
    if (document.head) {
      const style = document.createElement("style");

      // I really like the minimalistic look this style creates.
      style.textContent = `
      /* This thing is huge, irritating, and uncloseable if JS is not enabled. */
      .js-consent-banner,
      .js-add-answer-component,
      /* "Sign up" banner at the bottom of the page. */
      .js-dismissable-hero,
      #left-sidebar,
      #hot-network-questions,
      .s-sidebarwidget,
      #noscript-warning,
      .s-btn[href="/questions/ask"],
      .s-topbar,
      #announcement-banner,
      #feed-link,
      .js-toast,
      .site-header,
      .js-saves-btn,
      /* "Add a comment" link. */
      .js-add-link,
      .js-suggest-edit-post,
      .js-follow-post,
      footer,
      [data-s-popover-reference-selector="#trending-sort-info"] {
        display: none !important;
      }

      /* Shrink the large empty space at the top of the page. */
      .question-page {
        margin-top: 2.5rem !important;
        padding-top: 0 !important;
      }`;

      document.head.append(style);

      observer.disconnect();
    }
  }).observe(document, { childList: true });
})();
