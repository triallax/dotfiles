// ==UserScript==
// @match https://github.com/*
// @match https://gist.github.com/*
//
// @run-at document-start
// @grant none
// @qute-js-world jseval
// ==/UserScript==

(() => {
  new MutationObserver((mutations, observer) => {
    if (document.head) {
      const style = document.createElement("style");

      style.textContent = `
      #insights-tab,
      aside[aria-label="Explore"],
      header a[href="/explore"],
      header a[href="/marketplace"],
      header a[href="/codespaces"],
      /* "Following" and "For you" stuff in the home page. */
      #dashboard,
      /* The left sidebar in the home page. */
      .dashboard-sidebar,
      .js-yearly-contributions,
      #repo-notifications-counter,
      #repo-network-counter,
      .footer,
      /* Seems to pop up out of nowhere sometimes. */
      .js-skip-to-content {
        display: none !important;
      }

      /* Remove yellow background of mentions of the currently signed-in user. */
      .user-mention[href$="/mhmdanas"] {
        color: unset !important;
        background-color: unset !important;
        border-radius: unset !important;
        margin-left: unset !important;
        margin-right: unset !important;
        padding: unset !important;
      }

      /* GitHub changed the color of all issue close icons to the merge icon's
       * purple, which for many people including me is a rather major regression.
       * The following CSS stuff changes the colors back.
       * (https://github.blog/changelog/2021-10-26-updates-to-our-issue-status-icons-and-colors/)
       */

      .octicon.octicon-issue-closed.color-fg-done,
      .octicon.octicon-issue-closed.closed {
        color: var(--color-danger-fg) !important;
      }

      .State--merged[title="Status: Closed"], #show_issue .color-bg-done-emphasis {
        background-color: var(--color-danger-emphasis) !important;
      }`;

      document.head.append(style);

      observer.disconnect();
    }
  }).observe(document, { childList: true });
})();
