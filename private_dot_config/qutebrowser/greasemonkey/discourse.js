// ==UserScript==
// @match https://discourse.purescript.org/*
//
// @run-at document-start
// @grant none
// @qute-js-world jseval
// ==/UserScript==

(() => {
  new MutationObserver((mutations, observer) => {
    if (document.head) {
      const style = document.createElement("style");

      // Remove somewhat unnecessary social aspects.
      style.textContent = `
      .who-liked, .post-likes, .like-count {
        display: none !important;
      }`;

      document.head.append(style);

      observer.disconnect();
    }
  }).observe(document, { childList: true });
})();
