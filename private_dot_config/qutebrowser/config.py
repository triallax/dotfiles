import platform
import random
from qutebrowser.api import interceptor

from PyQt6.QtCore import QUrl, QUrlQuery

config.load_autoconfig()

########## Redirections ##########

# https://docs.invidious.io/instances/
invidious_instance = "invidious.fdn.fr"

invidious_url_params = {
    "dark_mode": "false",
    "autoplay": "0",
    "continue": "0",
    "quality": "medium",  # 480p
    "related_videos": "false",
    # Maybe also enable `thin_mode` and `listen`?
}

# Based on https://github.com/qutebrowser/qutebrowser/issues/5416#issuecomment-625233286.
def intercept(request):
    host = request.request_url.host()
    url_params = None

    if (
        host == "www.youtube.com"
        or host == "youtube.com"
        or host == "youtu.be"
        or host == "piped.kavin.rocks"
        or host == "piped.video"
    ):
        new_host = invidious_instance
        url_params = invidious_url_params

    elif host == "medium.com" or host.endswith(".medium.com"):
        # https://git.sr.ht/~edwardloveall/scribe/tree/main/docs/instances.md#instances
        new_host = "scribe.rip"

    elif host == "twitter.com" or host == "www.twitter.com":
        # https://github.com/zedeus/nitter/wiki/Instances#public
        new_host = "nitter.net"

    else:
        return

    new_url = QUrl(request.request_url)
    # Once the redirected URL used `http` for whatever reason, so I put
    # this in here.
    new_url.setScheme("https")
    new_url.setHost(new_host)

    if url_params is not None:
        url_query = QUrlQuery(new_url.query())

        for key, value in url_params.items():
            url_query.addQueryItem(key, value)

        new_url.setQuery(url_query)

    try:
        request.redirect(new_url)
    except interceptor.interceptors.RedirectFailedException:
        pass


interceptor.register(intercept)

########## Config ##########

system = platform.system()

if system == "Darwin":
    c.downloads.open_dispatcher = "open"
elif system == "Linux":
    c.downloads.open_dispatcher = "xdg-open"
    c.window.hide_decoration = True
else:
    if system == "Windows":
        print("Whatever you're doing, using Windows is not worth it.")
    else:
        print("What is this OS?")

    import sys

    sys.exit(1)

# Maybe I can put something more personal here later.
c.url.start_pages = "about:blank"
c.url.default_page = "about:blank"

searchengines = {
    "ddg": "https://lite.duckduckgo.com/lite?q={}",
    "wt": "https://en.wiktionary.org/w/index.php?search={}",
    "w": "https://en.wikipedia.org/w/index.php?search={}",
    "pub": "https://pub.dev/packages/{}",
    "pubc": "https://pub.dev/packages/{}/changelog",
    "pubs": "https://pub.dev/packages?q={}",
    "yt": f"https://{invidious_instance}/search?q={{}}",
}

searchengines["DEFAULT"] = searchengines["ddg"]

c.url.searchengines = searchengines

c.confirm_quit = ["multiple-tabs", "downloads"]

c.content.autoplay = False

c.tabs.position = "left"

c.downloads.location.prompt = False

# TODO: make this work on macOS (`nvim` is not in Qutebrowser's `PATH`, see
# https://github.com/qutebrowser/qutebrowser/issues/4273, but even if it was,
# Neovim needs to run in a terminal).
# c.editor.command = ["nvim", "{file}", "-c", "normal {line}G{column0}l"]

if system == "Linux":
    c.editor.command = ["foot", "-e", "hx", "{file}", "-c", "normal {line}G{column0}l"]

# TODO: make this work on macOS and, well, everywhere.
# file_select_command = ["alacritty", "-e", "nnn", "-p", "-"]
#
# c.fileselect.folder.command = file_select_command
# c.fileselect.multiple_files.command = file_select_command
# c.fileselect.single_file.command = file_select_command

c.fonts.default_size = "16pt"

c.content.pdfjs = False

c.tabs.title.format = "{audio}{current_title}"

# Disabling JIT compilation significantly reduces the attack surface of a
# browser, greatly increasing security. Quoting from
# https://microsoftedge.github.io/edgevr/posts/Super-Duper-Secure-Mode/:
#
# > Looking at CVE (Common Vulnerabilities and Exposures) data after 2019
# > shows that roughly 45% of CVEs issued for V8 were related to the JIT
# > engine. Moreover, we know that attackers weaponize and abuse these bugs
# > as well; an analysis from Mozilla shows that over half of the “in the
# > wild” Chrome exploits abused a JIT bug [...]
# Turns off WebAssembley currently, which I need.
# c.qt.args = ["js-flags=--jitless"]

########## Normal mode bindings ##########

config.bind("<Space>f", "fullscreen")
config.bind("<Space>;", "clear-messages")
config.bind("<Space>p", "spawn --detach --verbose umpv {url}")
config.bind("<Ctrl+/>", "hint url spawn --detach --verbose umpv {hint-url}")

########## Privacy stuff ##########

# This breaks Element, and `content.canvas_reading` cannot be configured
# per-domain at the moment.
# Qutebrowser issue: https://github.com/qutebrowser/qutebrowser/issues/5439
# c.content.canvas_reading = False

c.content.cookies.accept = "no-3rdparty"

# Taken from https://wiki.archlinux.org/title/Qutebrowser#Set_a_common_HTTP_ACCEPT_header.
c.content.headers.accept_language = "en-US,en;q=0.5"
c.content.headers.custom = {
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
}

c.content.images = False

c.content.javascript.enabled = False

# Workman baby.
config.set("hints.chars", "ashtneoi")

config.set(
    "hints.selectors",
    {
        **c.hints.selectors,
        "all": [*c.hints.selectors["all"], ".select-menu-item"],
    },
    "https://github.com/*",
)

config.set(
    "hints.selectors",
    {
        **c.hints.selectors,
        "all": [*c.hints.selectors["all"], ".button"],
    },
    "https://monkeytype.com/*",
)

config.set(
    "hints.selectors",
    {
        **c.hints.selectors,
        "all": [*c.hints.selectors["all"], ".react-numeric-input b"],
    },
    "https://didoesdigital.com/typey-type/*",
)

c.content.blocking.method = "both"

# TODO: see what I should choose for the following config options, if any:
# c.content.webrtc_ip_handling_policy = 'all-interfaces'
# c.downloads.position = 'top'
# c.new_instance_open_target = 'tab'
# c.new_instance_open_target_window = 'last-focused'
# c.scrolling.bar = 'overlay'
# c.session.default_name = None
# c.statusbar.position = 'bottom'
# c.statusbar.show = 'always'
# c.statusbar.widgets = ['keypress', 'url', 'scroll', 'history', 'tabs', 'progress']
# c.tabs.last_close = 'ignore'
# c.tabs.padding = {'top': 0, 'bottom': 0, 'left': 5, 'right': 5}
# c.tabs.select_on_remove = 'next'
# c.tabs.title.format_pinned = '{index}'
# c.tabs.wrap = True
