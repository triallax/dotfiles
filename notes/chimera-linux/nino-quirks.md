- I got a terrible generic battery for my laptop whose charge jumps down to 0% at around 60%. To have poweralertd warn when it gets close to that instead of when it reaches 0%, edit /etc/UPower/UPower.conf, setting Percentage{Low,Critical,Action} to appropriate values (70, 65, and 62 respectively in this case).
- To fix Capslock LED being on after resuming from suspend, run `sudo mkdir -p /etc/elogind/system-sleep` and put the following into a `capslock` script in that directory:

  ```sh
  #!/bin/sh -euf

  case "$1" in
  	post) echo 1 > /sys/class/leds/input0::capslock/brightness ;;
  esac
  ```

  And then `sudo chmod +x /etc/elogind/system-sleep/capslock`.
