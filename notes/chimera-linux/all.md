To set up a new Chimera Linux system (after installing Chimera itself):

## Users and groups

See https://chimera-linux.org/docs/configuration/post-installation.

Then log out of `root` and into `triallax`, and run

    # passwd -l root

To disable root login.

Some tips:
- Remove user from groups: `gpasswd --delete triallax foo`

## Wi-Fi

First, check that no relevant interfaces are blocked:

    $ rfkill

If any relevant interface is soft-blocked, unblock it;

    # rfkill unblock <interface>

If something is hard-blocked, check for anything like a Wi-Fi button and try
pressing it.

Then follow the steps in https://chimera-linux.org/docs/configuration/network#networkmanager.

## Clone dotfiles

    # apk add chezmoi
    $ chezmoi init https://codeberg.org/triallax/dotfiles
    $ chezmoi apply

## Bluetooth

See https://chimera-linux.org/docs/configuration/bluetooth.

## CPU microcode

See https://chimera-linux.org/docs/configuration/microcode.

## zram

Create `/etc/modules-load.d/zram.conf` with these contents:

    # load zram
    zram

and also `/etc/udev/rules.d/zram.rules` with these contents:

    ACTION=="add", KERNEL=="zram0", SUBSYSTEM=="block", ATTR{comp_algorithm}="zstd", ATTR{disksize}="3GiB", ATTR{mem_limit}="1GiB", RUN+="/sbin/mkswap -U clear $env{DEVNAME}"

Change 3GiB and 1GiB to values appropriate to the RAM size of your machine. In
my experience, the compression ration is roughly 3:1 to 5:1, at least with zstd.
Note that decimal values (e.g. `1.5GiB`) cannot be used with `disksize` and
`mem_limit`. Instead, use e.g. `1536MiB`.

Add this line to `/etc/fstab`:

    /dev/zram0	none	swap	defaults	0	0

Edit `/etc/default/grub`, adding these kernel options to the line containing
`GRUB_CMDLINE_LINUX_DEFAULT`:

    zswap.enabled=0 vm.swappiness=100

(Not sure if disabling zswap is needed. Also, need to figure out what value of
`vm.swappiness` works best.)

After doing that, run

    # update-initramfs -c -k all
    # update-grub

then reboot your system, and if nothing's exploded, check that zram is enabled:

    $ cat /proc/swaps

If one of the lines in the output begins with `/dev/zram0`, then voila! You've
got zram working!

Further reading:

- https://wiki.gentoo.org/wiki/Zram
- https://wiki.archlinux.org/title/ZRAM
- https://docs.kernel.org/admin-guide/blockdev/zram.html

## Firewall

See https://chimera-linux.org/docs/configuration/network/firewall#uncomplicated-firewall-ufw.

TL;DR:

    # ufw default deny
    # ufw default deny outgoing
    # ufw allow out http
    # ufw allow out https
    # ufw allow out dns
    # ufw allow out ntp
    # ufw allow out git
    # ufw allow syncthing
    # ufw allow out 6697/tcp # IRC/TLS
    # ufw allow out 465/tcp # SMTPS
    # ufw allow out 993/tcp # IMAPS
    # ufw allow out 22/tcp # SSH
    # ufw allow 1714:1764/udp
    # ufw allow 1714:1764/tcp
    # ufw allow out 8080 # Sometimes used for HTTP
    # ufw reload

## Printing

Note that these notes are for wireless printing with IPPS.

Install `cups` and `cups-filters` if not already installed and link the cupsd
service:

    # apk add cups cups-filters
    # dinitctl enable cupsd

Then depending on your printer's maker and model, you may need to install the
appropriate driver(s).

For Epson Inkjet printers:

    # apk add epson-inkjet-printer-escpr

Add your user to the `_lpadmin` group:

    # usermod -aG _lpadmin <user>

Depending on your printer model, you need to make it visible so that your
machine can connect to it (if you're having issues connecting, restarting
`cupsd` may also help wake it up). For Epson ET-2810, press on the Wi-Fi button
for a few seconds until the two lights beside it start flashing alternately.

Open localhost:631 in your browser, press on "Administration" then "Find New
Printers," press on the "Add This Printer" button besides the entry for your
printer, and then follow the steps. If you need to provide a PPD file, search
for it in /usr/share/ppd.

Once you're done, press on "Set Printer Options," and set the appropriate
options. Next, go to "Printers," then choose the printer you just added. Press
on the dropdown with "Maintenance" in it, then choose "Print Test Page." If
you've set up everything correctly, then voila, the printer should have printed
out the test page. Congrats!

## Console keymap

Download Workman keymap and copy it to appropriate location:

    $ cd
    $ curl -O https://raw.githubusercontent.com/workman-layout/Workman/master/linux_console/workman.iso15.kmap
    $ gzip workman.iso15.kmap
    # mkdir -p /usr/local/share/kbd/keymaps
    # mv workman.iso15.kmap.gz /usr/local/share/kbd/keymaps/workman.map.gz

Then add this line to /etc/rc.local:

    loadkeys -q -u /usr/local/share/kbd/keymaps/workman.map.gz

(Note to self: is `-u` needed? What does it do exactly?)

## Fingerprint authentication

See https://wiki.archlinux.org/title/Fprint.

Install `fprintd`, and add

    auth      sufficient pam_fprintd.so

to at least the top of the `auth` section of `/etc/pam.d/doas`.

## Plover

(note: Plover isn't packaged for Chimera Linux yet)

Add your user to the `dialout` group, so that Plover can access serial ports:

    # usermod -aG dialout triallax

And that's it pretty much!

## Miscellaneous

- Linux 6.1.4 broke something with backlight brightness control, at least on
  nino. I was able to fix it by adding `acpi_backlight=native` to the kernel
  parameters.
- Add `rfkill block bluetooth` to `/etc/rc.local` to stop Bluetooth from
  activating automatically on launch.
- Set "Notification content" to "Name only" in Signal Desktop.
- When setting up Syncthing, set the versioning for all synced folders to "Simple versioning."
- Set thresolds for when to start and stop charging in `tlp`: https://linrunner.de/tlp/faq/battery.html#how-to-enable-charge-thresholds
  Currently I use 85% and 90% respectively.
- You need to add your user to the `kvm` group for GNOME Boxes to work correctly.
- To set the keyboard layout in GDM to Workman, set `XkbLayout` to `us` and
  `XkbVariant` to `workman` in `/etc/X11/xorg.conf.d/00-keyboard.conf`.
