Recommended reading:
- https://unix.stackexchange.com/a/474617

Steps:

    # mkfs.ext4 /dev/sda1
    # mkdir /mnt/backup
    # mnt /dev/sda1 /mnt/backup
    # restic init -r /mnt/backup
    # umount /mnt/backup

Now create `/etc/udev/rules.d/backup.rules` with this content:

```udev
ACTION=="add", SUBSYSTEM=="block", ENV{ID_PART_ENTRY_UUID}=="<partuuid>", SYMLINK="backup1", RUN+="/usr/local/bin/backup"
```

(As for how to find the partuuid, idk figure it out)
(Also `backup1` because I'll probably get another backup drive at some point)

Then create an executable file `/usr/local/bin/backup`:

```sh
#!/bin/sh -euf

exec >/tmp/backup.log
exec 2>&1

export XDG_CACHE_HOME=/var/cache

echo "$(date) - Mounting /dev/backup1 to /mnt/backup"
mkdir -p /mnt/backup
mount /dev/backup1 /mnt/backup
echo "$(date) - Starting backup of / to /mnt/backup"
restic -r /mnt/backup -p /etc/backup1-password.txt --exclude-file /etc/backup-excludes.txt backup / --read-concurrency 10
echo "$(date) - Unmounting /mnt/backup"
umount /mnt/backup
```

And write whatever excludes to `/etc/backup-excludes.txt`, and the repo password
to `/etc/backup1-password.txt`. Also, run

    # chmod 000 /etc/backup1-password.txt

so that the password isn't readable by non-root users.
