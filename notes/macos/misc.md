- By default, `/bin/sh` uses macOS's ancient Bash 3.2, which Apple has no plans
  of updating. To make it use macOS's Dash instead, run `sudo ln -sf /bin/dash /var/select/sh`.
