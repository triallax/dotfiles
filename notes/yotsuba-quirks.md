Kernel parameters:
- `libata.force=1.00:disable` (internal hard drive is busted)
- `amdgpu.dpm=0` to make boot, display config changes, and a bunch of other things MUCH faster and to unbreak screen wakeup (huge thanks to OP of https://forums.linuxmint.com/viewtopic.php?t=370260 for this tip)
