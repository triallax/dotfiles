Follow these steps when setting up a new machine:
1. Choose and set hostname (not possible on Android with Termux unfortunately)
2. After cloning the dotfiles using Chezmoi, go into the Chezmoi source dir and
   run `git grep --files-with-matches -E '(hostname|chezmoi\.os|osRelease)' | xargs "$EDITOR" --`
   to see files with machine-specific contents, and check that their contents
   are appropriate for this new machine too, or edit them otherwise.
